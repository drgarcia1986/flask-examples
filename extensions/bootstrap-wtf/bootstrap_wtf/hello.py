#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, flash, redirect, url_for
from flask.ext.bootstrap import Bootstrap
from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Email

app = Flask(__name__)
app.config['SECRET_KEY'] = 'a7b05c4e06fe0502af4a3d42dd41327b'
Bootstrap(app)


class ContactForm(Form):
    name = StringField("Name", validators=[DataRequired()])
    email = StringField("E-mail", validators=[Email()])
    msg = TextAreaField("Message")
    submit = SubmitField("Submit")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/contact', methods=['GET', 'POST'])
def contact():
    form = ContactForm()
    if form.validate_on_submit():
        flash('Thank you %s' % form.name.data)
        return redirect(url_for('index'))
    return render_template('contact.html', form=form)


if __name__ == "__main__":
    app.run(debug=True)