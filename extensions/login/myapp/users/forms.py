# encoding: utf-8
from flask.ext.wtf import Form
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import (
    StringField,
    SubmitField,
    PasswordField,
    BooleanField,
)
from myapp.utils.form_validators import Unique
from models import User


class SignUp(Form):
    name = StringField('Name', validators=[DataRequired()])

    email = StringField(
        'E-mail address',
        validators=[
            DataRequired(),
            Email(),
            Unique(
                model=User,
                field=User.email,
                message='There is already an account with that email.'
            )
        ]
    )

    password = PasswordField('Password', validators=[DataRequired()])
    confirm = PasswordField(
        'Confirm your password',
        validators=[EqualTo('password', message='Password must match.')]
    )

    submit = SubmitField('Sign Up')


class SignIn(Form):
    email = StringField('E-mail', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('remember me')
    submit = SubmitField('Sign in')
