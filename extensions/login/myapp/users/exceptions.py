# encoding: utf-8


class InvalidLogin(Exception):
    http_status = 401
    message = 'Invalid e-mail address or password'
