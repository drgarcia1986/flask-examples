# encoding: utf-8
from flask.ext.login import login_user
from myapp import db, login_manager

from exceptions import InvalidLogin


class User(db.Model):
    __tablename__ = 'user'
    email = db.Column(db.String(128), primary_key=True)
    name = db.Column(db.String(128))
    password = db.Column(db.String(128))

    def __init__(self, email, name, password):
        self.email = email
        self.name = name
        self.password = password

    @classmethod
    def login(cls, email, password, remember):
        user = cls.query.filter_by(
            email=email,
            password=password
        ).first()

        if not user:
            raise InvalidLogin()

        login_user(user, remember=remember)
        return True

    @classmethod
    def create(cls, user):
        db.session.add(user)
        db.session.commit()
        login_user(user)

    def is_active(self):
        return True

    def get_id(self):
        return self.email

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def __repr__(self):
        return '<User {} ({})'.format(self.name, self.email)


@login_manager.user_loader
def load_user(email):
    return User.query.get(email)
