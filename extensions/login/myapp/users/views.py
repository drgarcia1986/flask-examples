# encoding: utf-8
from flask import Blueprint, render_template, flash, redirect
from flask.ext.login import login_required, logout_user

from forms import SignIn, SignUp
from models import User
from exceptions import InvalidLogin


users_blueprint = Blueprint(
    'users',
    __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/static/users'
)


@users_blueprint.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SignIn()
    if form.validate_on_submit():

        try:
            User.login(
                email=form.email.data,
                password=form.password.data,
                remember=form.remember.data
            )

            print form.remember.data

        except InvalidLogin as e:
            flash(e.message, category='danger')
            return redirect('/signin')

        return redirect('/home')
    return render_template('signin.html', form=form)


@users_blueprint.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUp()
    if form.validate_on_submit():

        User.create(
            User(
                email=form.email.data,
                name=form.name.data,
                password=form.password.data
            )
        )

        return redirect('/home')
    return render_template('signup.html', form=form)


@users_blueprint.route('/home')
@login_required
def home():
    return render_template('home.html')


@users_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')
