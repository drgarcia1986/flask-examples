# encoding: utf-8
import os


class Base(object):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SECRET_KEY = os.environ.get('SECRET_KEY')


class Development(Base):
    DEBUG = True
    DEVELOPMENT = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///tmp.db'
    SECRET_KEY = '807d033e-f1ed-11e4-97bf-1078d2624365'
