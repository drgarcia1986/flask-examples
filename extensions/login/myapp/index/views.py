# encoding: utf-8
from flask import Blueprint, render_template, redirect
from flask.ext.login import current_user

index_blueprint = Blueprint(
    'index',
    __name__,
    template_folder='templates'
)


@index_blueprint.route('/')
def index():
    if current_user.is_authenticated():
        return redirect('/home')
    return render_template('index.html')
