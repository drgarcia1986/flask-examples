# encoding: utf-8
from flask import Flask
from flask.ext.bootstrap import Bootstrap
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager

# APP
app = Flask(__name__)
app.config.from_object('myapp.settings.Development')

# Exts
Bootstrap(app)

login_manager = LoginManager(app)
login_manager.login_view = 'users.signin'

db = SQLAlchemy(app)
# FIXME
from myapp.users.models import User
try:
    db.create_all()
    db.session.commit()
except:
    pass

# Blueprints
from myapp.users import users_blueprint
from myapp.index import index_blueprint
app.register_blueprint(users_blueprint)
app.register_blueprint(index_blueprint)
