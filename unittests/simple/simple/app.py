#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify, abort

app = Flask(__name__)


@app.route('/person/<int:uid>')
def person(uid):
    if uid != 1:
        abort(404)
    return jsonify(name='Foo', email='foo@bar.net')


if __name__ == "__main__":
    app.run()

