#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import app
import unittest
from json import loads


class TestApp(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()

    def test_person(self):
        resp = self.client.get('/person/1')
        resp_data = loads(resp.data.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp_data['name'], 'Foo')
        self.assertEqual(resp_data['email'], 'foo@bar.net')

    def test_person_not_found(self):
        resp = self.client.get('/person/2')
        self.assertEqual(resp.status_code, 404)


if __name__ == '__main__':
    unittest.main()
